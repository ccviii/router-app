
var socket = io.connect();

angular.module('app', [])
.controller("mainController", function($scope, $http) {
	$scope.nodes = []

	$http.get('/nodes').then(function(res){
		$scope.nodes = res.data;
	})
})