/**
 *	Channel List
 * 	- Store
 */

var Reflux 	= require('reflux');
var $ 		= require('jquery');
var nodesActions 	= require('./actions');

module.exports = Reflux.createStore({
	nodes: [],
	listenables: [nodesActions],
	init: function(){
		this.fetchNodes();
	},
	fetchNodes: function() {
		$.ajax({
			url: '/nodes',
			method: 'get',
			success: data => {
				this.nodes = data;
				this.trigger(this.nodes)
			}
		})
	},
	deleteNode: function(){
		this.fetchNodes();
	},
	getNodes: function(cb){
		if(typeof cb == "function")
			cb(this.nodes);
	}
})