// dependencies
var Reflux = require('reflux');

module.exports = Reflux.createActions([
	'getNodes',
	'deleteNode',
	'fetchNodes'
]);