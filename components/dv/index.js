/**
 *	UserList
 * 	- Main Component
 */

import Reflux from 'reflux';
import React from 'react';
import NodesStore from './store';

module.exports = React.createClass({
	mixins: [Reflux.connect(NodesStore, 'nodesstore')],
	render: function() {
		let nodes = (<tr></tr>);
		
		if(this.state.nodesstore){
			nodes = this.state.nodesstore.map(node => {
				return (
					<tr>
						<td>{node.name}</td>
						<td>{node.via}</td>
						<td>{node.cost}</td>
					</tr>		
				)
			})
		}
		
		return (
			<div>
				<h2>DV / Forwarding</h2>
				<table className="table">
					<thead>
						<th>Dest</th>
						<th>Via</th>
						<th>Cost</th>
					</thead>
					<tbody>
					{nodes}
					</tbody>
				</table>
			</div>
		)
	}
})