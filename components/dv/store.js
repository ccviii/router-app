/**
 *	Channel List
 * 	- Store
 */

var Reflux 	= require('reflux');
var $ 		= require('jquery');
var nodesActions 	= require('./actions');

module.exports = Reflux.createStore({
	dv: {},
	listenables: [nodesActions],
	init: function(){
		this.getDV();
	},
	getDV: function() {
		$.ajax({
			url: '/dv',
			method: 'get',
			success: data => {
				this.dv = data;
				this.trigger(this.dv)
			}
		})
	},
	updateDV: function(){
		this.getDV();
	}
})