/**
 *	Messages
 * 	- Main Component
 */

import React from 'react';
import Reflux from 'reflux';
// import _ from 'lodash';
// import $ from 'jquery';
// import toastr from 'toastr'

// import NodesActions from '../nodes/actions';
import MessagesStore from './store'

module.exports = React.createClass({
	mixins: [Reflux.connect(MessagesStore, 'messages')],
	render: function() {
		let messages = [];


		if(this.state.messages){
			messages = this.state.messages.map(message => {
				return (
					<tr>
						<td>{message.from}</td>
						<td>{message.to}</td>
						<td>{message.msg}</td>
						<td>{message.status}</td>
					</tr>
				)
			})
		}

		return (
			<div>
				<h2>Messages</h2>
				<table className="table">
					<thead>
						<th>From</th>
						<th>To</th>
						<th>message</th>
						<th>status</th>
					</thead>
					<tbody>
						{messages}
					</tbody>
				</table>
			</div>
		);
	}
})