// dependencies
import React from 'react';
import ReactDOM from 'react-dom'

import Nodes from './nodes/'
import NodesActions from './nodes/actions'

import Dv from './dv/'
import DvActions from './dv/actions'

import MessageBox from './messagebox/'
import Messages from './messages/'
import MessagesActions from './messages/actions'

var socket = io.connect();

ReactDOM.render(
	<Nodes />,
	document.getElementById('table')
)
ReactDOM.render(
	<Dv />,
	document.getElementById('dv')
)
ReactDOM.render(
	<MessageBox />,
	document.getElementById('messageBox')
)
ReactDOM.render(
	<Messages />,
	document.getElementById('messages')
)


// SOCKETS
socket.on('update:nodes', data => {
	//por ahora
	NodesActions.deleteNode();
})

// SOCKETS
socket.on('update:dv', data => {
	//por ahora
	DvActions.updateDV();
})

socket.on('update:messages', data => {
	let alert = {};
	if(data.new_val){
		if(data.new_val.status=="success"){
			notification(data.new_val.to+' [Foward]', data.new_val.msg)
			alert = {
				title: 'Foward!',
				msg: `From:${data.new_val.from} To: ${data.new_val.to}<br>${data.new_val.msg}`,
				type: 'success'
			}
		} else if(data.new_val.status=="fail"){
			alert = {
				title: 'Foward fail!',
				msg: `From:${data.new_val.from} To: ${data.new_val.to}<br>${data.new_val.msg}`,
				type: 'warning'
			}
		} else if(data.new_val.status=="recieved"){
			notification(data.new_val.from, data.new_val.msg)
			alert = {
				title: 'Recieved!',
				msg: `From: ${data.new_val.from}<br>${data.new_val.msg}`,
				type: 'success'
			}
		} else {
			console.log(data.new_val);
		}
	}

	MessagesActions.update(alert);
})


// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
	if(typeof Notification == "undefined") return;
  	else if(Notification.permission !== "granted")
    	Notification.requestPermission();
});

function notifyMe() {
	if(typeof Notification == "undefined") return;
  	
  	else if (!Notification) {
    	alert('Desktop notifications not available in your browser. Try Chromium.'); 
    	return;
  	}

  	if(Notification.permission !== "granted")
		Notification.requestPermission();
}
notifyMe();

function notification(name, body){
	var options = {
	  icon: "http://api.adorable.io/avatars/80/"+name+"%40adorable.io",
	  body: body
	 };

	var _notification = new Notification(name,options);

	_notification.onclick = function () {
		window.focus();
	}

	_notification.onshow = function () {
	  // play sound on show
	  var myAud=document.getElementsByTagName("audio")[0];
	  myAud.play();

	  // auto close after 1 second
	  
	}

	setTimeout(function() {
		_notification.close();
	}, 3000);

}