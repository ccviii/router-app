/**
 *	Messages
 * 	- Main Component
 */

import React from 'react';
import Reflux from 'reflux';
import _ from 'lodash';
import $ from 'jquery';

import NodesActions from '../nodes/actions';
import NodesStore from '../dv/store'
import toastr from 'toastr'



module.exports = React.createClass({
	mixins: [Reflux.connect(NodesStore, 'nodesstore')],
	handleSubmit: function(e){
		e.preventDefault();
		if(this.state.to==undefined || this.state.message==undefined || this.state.to==""){
			toastr.warning('Mising Params')
			return;
		} else {
			$.ajax({
				url: '/send-message',
				method: 'POST',
				data: {message: this.state.message, to: this.state.to},
				success: data => {
					if(data.code==0)
						toastr.success('Message Sent!', `To: ${this.state.to} - ${this.state.message}`)
					else
						toastr.error('Unable to sent message', `To: ${this.state.to} - ${this.state.message}`)
					this.setState({to: '', message: ''})
				}
			})
		}

		
	},
	onChange: function(e){
		this.setState({message: e.target.value})
	},
	onChangeTo: function(e){
		this.setState({to: e.target.value})
	},
	render: function() {
		let nodes = (<option value="">No hay nodos disponibles</option>)
		let size = 0;
		if(this.state.nodesstore){
			// console.log(this.state.nodesstore)
			let result = _.filter(this.state.nodesstore, node => node.via!='!');
			size = result.length
			nodes = (result.length==0)? nodes: result.map(node => {
				return (
					<option value={node.name}>{node.name}</option>
				)
			})
		}

		let disabled = size==0;

		return (
			<div>
				<h2>Mandar Mensaje</h2>
				<form onSubmit={this.handleSubmit}>
				  <div className="form-group">
				    <label>TO:</label>
				    <select onChange={this.onChangeTo} value={this.state.to} className="form-control">
				    	<option value="">--</option>
				    	{nodes}
				    </select>
				  </div>
				  <div className="form-group">
				    <label>Message</label>
				    <textarea onChange={this.onChange} value={this.state.message} placeholder="Hola mensaje de prueba" className="form-control" />
				  </div>
				  <div className="form-group pull-right hidden-xs">
				    <button disabled={disabled} type="submit" className="btn btn-block btn-primary">Enviar!</button>
				  </div>
				  <div className="form-group visible-xs-block">
				    <button disabled={disabled} type="submit" className="btn btn-block btn-primary">Enviar!</button>
				  </div>
				</form>
			</div>
		)
	}
})