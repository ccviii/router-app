// dependencies
var Reflux = require('reflux');

module.exports = Reflux.createActions([
	'fetchMessages',
	'update'
]);