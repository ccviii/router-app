 // 
 //	SOCKETS
 //

var r = require('rethinkdb')
var _ = require('lodash')
var users = [];

module.exports = {
	_: function(socket){
		console.log("connected");

		socket.on('disconnect', function(){
			console.log("disconnected")
			var user = _.filter(users, function(user){
					return user.id==socket.id
				})[0];

			users = _.filter(users, function(user){
				return user.id!=socket.id
			})
			
			if(user)
				user.status = "away";
			socket.broadcast.emit('to.client.user.update', user);
		})

		socket.emit('welcome', users)

		socket.on('to.server.new.user', function(name){
			var user = {id:socket.id, name: name, status: 'online'};
			users.push(user);
			socket.broadcast.emit('to.client.new.user', user);
		})

		socket.on('to.server.user.update', function(user){
			console.log(user)
			socket.broadcast.emit('to.client.user.update', user);
		})
	},
	custom: function(io){
		// Listening Changes
		r.connect(webjs.db.rethinkdb).then(function(conn){
			return r.db(webjs.db.database).table('nodes').changes().run(conn);
		}).then(function(cursor){
			cursor.each(function(err, item){
				io.sockets.emit('update:nodes', item);
			})
		})

		r.connect(webjs.db.rethinkdb).then(function(conn){
			return r.db(webjs.db.database).table('dv').changes().run(conn);
		}).then(function(cursor){
			cursor.each(function(err, item){
				io.sockets.emit('update:dv', item);
			})
		})

		r.connect(webjs.db.rethinkdb).then(function(conn){
			return r.db(webjs.db.database).table('messages').changes().run(conn);
		}).then(function(cursor){
			cursor.each(function(err, item){
				io.sockets.emit('update:messages', item);
			})
		})

	}
}
/*

Server-side

socket.broadcast.emit(event) // send everyone except for that socket
socket.on(event, fn)
socket.emit(event)

*//*

Client-side

html
<script src="/socket.io/socket.io.js"></script>
<script>
  var socket = io.connect('http://localhost');
  // socket.on(event, fn);
  // socket.emit(event);
</script>

jade
script(src="/socket.io/socket.io.js")
script var socket = io.connect('http://localhost:3000');
*/