/*
 * Routes
 */

module.exports = {
	'/': 'MainController.index',
	'GET /nodes': 'nodesApi.find',
	'GET /dv': 'nodesApi.DV',
	'POST /send-message': 'nodesApi.sendMessage',
	'POST /update-node': 'nodesApi.updateNode',
	'GET /messages': 'nodesApi.findMessages'
}