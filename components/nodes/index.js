/**
 *	UserList
 * 	- Main Component
 */

import React 	from 'react';
import Reflux 	from 'reflux';
import swal from 'sweetalert';
import $ from 'jquery'
import NodesStore from './store'
import toastr from 'toastr'

module.exports = React.createClass({
	mixins: [Reflux.connect(NodesStore, 'nodesstore')],
	handle: function(node, e){
		if(e.target.className=='info') {
			swal({
				title: "Change node's ip",
				text: "Enter node's new ip address",
				type: "input",
				showCancelButton: true,
				closeOnConfirm: false,
				inputPlaceholder: node.ip
			}, 
				function(inputValue){
					if (inputValue === false) return false;
					if (inputValue === "") {
						swal.showInputError("You need to write something!");
						return false
					}
					let old = node.ip;
					node.ip = inputValue;

					$.ajax({
						url: '/update-node',
						method: 'POST',
						data: {node: JSON.stringify(node)},
						success: ()=>{
							swal.close();
							toastr.success(`change ${old} to ${node.ip}`, 'Updated Ip!')
							// swal("Saved!", "new ip: " + inputValue, "success");
						},
						error: () => {
							swal.close();
							toastr.error('Unable to save')
						}
					})
				}
			);
		} else {
			swal({
				title: "Change node's name",
				text: "Enter node's new name",
				type: "input",
				showCancelButton: true,
				closeOnConfirm: false,
				inputPlaceholder: node.name
			}, 
				function(inputValue){
					if (inputValue === false) return false;
					if (inputValue === "") {
						swal.showInputError("You need to write something!");
						return false
					}
					let old = node.name;
					node.name = inputValue;

					$.ajax({
						url: '/update-node',
						method: 'POST',
						data: {node: JSON.stringify(node)},
						success: ()=>{
							swal.close();
							toastr.success(`change ${old} to ${node.name}`, 'Updated Ip!')
							// swal("Saved!", "new ip: " + inputValue, "success");
						},
						error: () => {
							swal.close();
							toastr.error('Unable to save')
						}
					})
				}
			);
		}
	},
	render: function() {
		var nodos = (<tr><td>No hay</td></tr>);
		if(this.state.nodesstore) {
			let self = this;
			 nodos = this.state.nodesstore.map( function(node) {
			 	let status = (node.status)? 'node node-online' : 'node node-away';
			 	let sent_hello = (node.hello.sent)? 'box-notify box-online': 'box-notify box-away';
			 	let sent_welcome = (node.welcome.sent)? 'box-notify box-online': 'box-notify box-away';
			 	let recieved_hello = (node.hello.recieved)? 'box-notify box-online': 'box-notify box-away';
			 	let recieved_welcome = (node.welcome.recieved)? 'box-notify box-online': 'box-notify box-away';
			 	let cost = (node.status)? node.cost: '-';

				return (
					<div className={status}>
					  <span className="badge">{cost}</span>
					  <div onDoubleClick={self.handle.bind(this, node)} className="title">
					    <h2>{node.name}</h2>
					  </div>
					  <div onDoubleClick={self.handle.bind(this, node)} className="info">{node.ip}</div>
					  <div className="row">
					    <div className="col-sm-4 col-sm-offset-2">
					    	<span>H</span>
					      	<div className={sent_hello}></div>
					    </div>
					    <div className="col-sm-4">
					    	<span>H</span>
					      	<div className={recieved_hello}></div>
					    </div>
					  </div>
					  <div className="row">
					    <div className="col-sm-4 col-sm-offset-2">
					    	<span>W</span>
					      	<div className={recieved_welcome}></div>
					    </div>
					    <div className="col-sm-4">
					    	<span>W</span>
					      	<div className={sent_welcome}></div>
					    </div>
					  </div>
					</div>
				)
			})
		}

		return (
			<div>
				<h2>Nodos</h2>
				{nodos}
			</div>
		)
	}
})