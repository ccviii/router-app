
/**
 * Messages - Store
 */
import Reflux from 'reflux';
import messagesActions from './actions';
import $ from 'jquery';
import toastr from 'toastr';

module.exports = Reflux.createStore({
	listenables: [messagesActions],
	messages: [],
	init: function(){
		this.fetchMessages();
	},
	fetchMessages: function(){
		$.ajax({
			url: '/messages',
			method: 'GET',
			success: data => {
				this.messages = data;
				this.trigger(this.messages)
			}
		})
	},
	update: function(alert){
		$.ajax({
			url: '/messages',
			method: 'GET',
			success: data => {
				this.messages = data;
				this.trigger(this.messages)
				if(alert.type){
					toastr[alert.type](alert.title, alert.msg);
				} else {
					toastr.success("New Message")
				}
				tm(true)
			}
		})	
	}
})